@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Posts') }} <a href="{{ url('post/create') }}"
                                                                  class="btn btn-success float-right">Create Post</a>
                    </div>

                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($posts))
                                @foreach($posts as $post)
                                    <tr>
                                        <td>{{ $post->title }}</td>
                                        <td>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <a href="{{ url('post/'.$post->id.'/edit') }}">Edit</a>
                                                </div>
                                                <div class="col-md-3">
                                                    <form action="{{ url('post/'.$post->id) }}" method="post">
                                                        @csrf
                                                        @method('delete')
                                                        <div class="form-group row mb-0">
                                                            <div class="col-md-2">
                                                                <button type="submit" class="btn btn-primary">
                                                                    {{ __('Delete') }}
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
